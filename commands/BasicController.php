<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Aluno;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BasicController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }


    public function actionCriarAlunos(){
        $fileHandler=fopen(\Yii::getAlias('@webroot')."/files/alunos.csv",'r');

        if($fileHandler){
            $i=0;
            while($line=fgetcsv($fileHandler,1000)){
                $i++;
                $model = new Aluno();
                $data_nascimento = \DateTime::createFromFormat('d/m/Y', trim($line[1]));
                $nome = trim($line[0]);
                $model->nome = $nome;


                if($data_nascimento)
                    $model->data_nascimento = $data_nascimento->format('Y-m-d');

                if($data_nascimento){
                    if($model->save()){
                        echo $i." - Aluno ". $model->nome." cadastrado".PHP_EOL;
                    }
                    else{
                        echo $i." - ERRO: Aluno ". $model->nome." não cadastrado".PHP_EOL;
                    }
                }


            }
        }
    }
}
