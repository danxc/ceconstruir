<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluno_nota".
 *
 * @property int $id
 * @property int $matricula_id
 * @property int $materia_id
 * @property double $nota
 * @property int $unidade
 *
 * @property Matricula $matricula
 * @property Materia $materia
 */
class AlunoNota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aluno_nota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula_id', 'materia_id', 'nota', 'unidade'], 'required'],
            [['matricula_id', 'materia_id', 'unidade'], 'integer'],
            [['nota'], 'number'],
            [['matricula_id'], 'exist', 'skipOnError' => true, 'targetClass' => Matricula::className(), 'targetAttribute' => ['matricula_id' => 'id']],
            [['materia_id'], 'exist', 'skipOnError' => true, 'targetClass' => Materia::className(), 'targetAttribute' => ['materia_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula_id' => 'Matricula ID',
            'materia_id' => 'Materia ID',
            'nota' => 'Nota',
            'unidade' => 'Unidade',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatricula()
    {
        return $this->hasOne(Matricula::className(), ['id' => 'matricula_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMateria()
    {
        return $this->hasOne(Materia::className(), ['id' => 'materia_id']);
    }
}
