<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aluno".
 *
 * @property int $id
 * @property string $nome
 * @property string $data_nascimento
 * @property string $endereco
 * @property string $telefone
 * @property string $email
 *
 * @property Matricula[] $matriculas
 * @property Responsavel[] $responsavels
 */
class Aluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'data_nascimento'], 'required'],
            [['data_nascimento'], 'safe'],
            [['nome', 'endereco', 'telefone', 'email'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'data_nascimento' => 'Data Nascimento',
            'endereco' => 'Endereco',
            'telefone' => 'Telefone',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::className(), ['aluno_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsavels()
    {
        return $this->hasMany(Responsavel::className(), ['aluno' => 'id']);
    }
}
