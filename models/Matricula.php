<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matricula".
 *
 * @property int $id
 * @property int $turma_id
 * @property int $aluno_id
 * @property string $data
 *
 * @property AlunoNota[] $alunoNotas
 * @property Aluno $aluno
 * @property Turma $turma
 */
class Matricula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matricula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turma_id', 'aluno_id', 'data'], 'required'],
            [['turma_id', 'aluno_id'], 'integer'],
            [['data'], 'safe'],
            [['aluno_id'], 'exist', 'skipOnError' => true, 'targetClass' => Aluno::className(), 'targetAttribute' => ['aluno_id' => 'id']],
            [['turma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Turma::className(), 'targetAttribute' => ['turma_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turma_id' => 'Turma ID',
            'aluno_id' => 'Aluno ID',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlunoNotas()
    {
        return $this->hasMany(AlunoNota::className(), ['matricula_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluno()
    {
        return $this->hasOne(Aluno::className(), ['id' => 'aluno_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(Turma::className(), ['id' => 'turma_id']);
    }
}
