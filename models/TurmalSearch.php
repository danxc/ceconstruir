<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Turma;

/**
 * TurmalSearch represents the model behind the search form of `app\models\Turma`.
 */
class TurmalSearch extends Turma
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'carga_horaria', 'curso_id'], 'integer'],
            [['data_inicio', 'data_final'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Turma::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'data_inicio' => $this->data_inicio,
            'data_final' => $this->data_final,
            'carga_horaria' => $this->carga_horaria,
            'curso_id' => $this->curso_id,
        ]);

        return $dataProvider;
    }
}
