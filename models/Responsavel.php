<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "responsavel".
 *
 * @property int $id
 * @property string $nome
 * @property string $empresa
 * @property string $telefone
 * @property int $aluno
 * @property string $rg
 * @property string $cpf
 *
 * @property Aluno $aluno0
 */
class Responsavel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'responsavel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'telefone', 'aluno'], 'required'],
            [['aluno'], 'integer'],
            [['nome', 'empresa', 'telefone', 'rg', 'cpf'], 'string', 'max' => 45],
            [['aluno'], 'exist', 'skipOnError' => true, 'targetClass' => Aluno::className(), 'targetAttribute' => ['aluno' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'empresa' => 'Empresa',
            'telefone' => 'Telefone',
            'aluno' => 'Aluno',
            'rg' => 'Rg',
            'cpf' => 'Cpf',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluno0()
    {
        return $this->hasOne(Aluno::className(), ['id' => 'aluno']);
    }
}
