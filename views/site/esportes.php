<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Esportes - Centro Educacional Construir';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pager-header">
    <div class="container">
        <div class="page-content">
            <h2>Esportes</h2>
            <p>Esportes na Construir</p>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::base()?>">Home</a></li>
                <li class="breadcrumb-item active">Esportes</li>
            </ol>
        </div>
    </div>
</div><!-- /Page Header -->

<div class="about-inner bg-grey padding">
    <div class="container">
        <div class="row about-inner-wrap">
            <div class="col-md-6 xs-padding">
                <div class="about-inner-content">
                    <h2>Ballet</h2>
                    <p style="align: justify">As atividades esportivas são extremamente importantes para os seres humanos. Além de serem fontes de vida e alegria, servem de elo na aproximação das pessoas no desenvolvimento do seu lado físico, psíquico e motor.</p>

                </div>
            </div>
            <div class="col-md-6 xs-padding">
                <div class="about-inner-bg">
                    <img src="<?=Url::base()?>/img/Ballet.png" alt="img">
                </div>
            </div>
        </div>
    </div>
</div><!-- /About Section -->
