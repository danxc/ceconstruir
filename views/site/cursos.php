<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Sobre - Centro Educacional Construir';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pager-header">
    <div class="container">
        <div class="page-content">
            <h2>Sobre nós</h2>
            <p>Veja um pouco sobre nossa escola </p>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?=Url::base()?>">Home</a></li>
                <li class="breadcrumb-item active">Sobre nós</li>
            </ol>
        </div>
    </div>
</div><!-- /Page Header -->

<div class="about-inner bg-grey padding">
    <div class="container">
        <div class="row about-inner-wrap">
            <div class="col-md-6 xs-padding">
                <div class="about-inner-content">
                    <h2>Quem nós somos?</h2>
                    <p style="align: justify">O Centro Educacional Construir é um espaço de formação e informação que busca oportunizar aprendizagens significativas aos educandos, instigando-os a desenvolver uma visão crítica, reflexiva, ética e estética, de sensibilidade, da ludicidade, da criatividade e da liberdade de expressão, nas diferentes manifestações artísticas do mundo, respeitando a fase de desenvolvimento dos mesmos e a realidade que estão inseridos, para que sejam agentes participativos de transformações sociais.</p>
                    <a href="#" class="default-btn">Pré matrícula</a>
                </div>
            </div>
            <div class="col-md-6 xs-padding">
                <div class="about-inner-bg">
                    <img src="<?=Url::base()?>/img/about-bg.jpg" alt="img">
                </div>
            </div>
        </div>
    </div>
</div><!-- /About Section -->
