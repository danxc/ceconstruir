<?php

/* @var $this yii\web\View */

$this->title = 'Centro Educacional Construir || Ensino infantil em Cajazeiras';
Yii::$app->params['description'] = 'Ensino infantil | Situado no bairro de cajazeiras'
?>
<section class="slider-section">
    <div class="slider-wrapper">
        <div id="main-slider" class="nivoSlider">
            <img src="img/slider-1.jpg" alt="" title="#slider-caption-1"/>
            <img src="img/slider-2.jpg" alt="" title="#slider-caption-2"/>
            <img src="img/slider-3.jpg" alt="" title="#slider-caption-3"/>
        </div><!-- /#main-slider -->

        <div id="slider-caption-1" class="nivo-html-caption slider-caption">
            <div class="display-table">
                <div class="table-cell">
                    <div class="container">
                        <div class="slider-text">
                            <h5 class="wow cssanimation fadeInBottom">Junte-se a nós!</h5>
                            <h1 class="wow cssanimation fadeInTop" data-wow-delay="1s" data-wow-duration="800ms">O sucesso começa aqui!</h1>
                            <p class="wow cssanimation fadeInBottom" data-wow-delay="1s">Ninguém ignora tudo. Ninguém sabe tudo. Todos nós sabemos alguma coisa. <br>Todos nós ignoramos alguma coisa. Por isso aprendemos sempre.</p>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Junte-se a nós</a>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Nossas Turmas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /#slider-caption-1 -->
        <div id="slider-caption-2" class="nivo-html-caption slider-caption">
            <div class="display-table">
                <div class="table-cell">
                    <div class="container">
                        <div class="slider-text">
                            <h5 class="wow cssanimation fadeInBottom">Junte-se a nós</h5>
                            <h1 class="wow cssanimation fadeInTop" data-wow-delay="1s" data-wow-duration="800ms">O sucesso começa aqui!</h1>
                            -<p class="wow cssanimation fadeInBottom" data-wow-delay="1s">Se a educação sozinha não transforma a sociedade,<br>sem ela tampouco a sociedade muda.</p>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Junte-se a nós</a>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Nossas Turmas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /#slider-caption-2 -->
        <div id="slider-caption-3" class="nivo-html-caption slider-caption">
            <div class="display-table">
                <div class="table-cell">
                    <div class="container">
                        <div class="slider-text">
                            <h5 class="wow cssanimation fadeInBottom">Junte-se a nós</h5>
                            <h1 class="wow cssanimation fadeInTop" data-wow-delay="1s" data-wow-duration="800ms">O sucesso começa aqui!</h1>
                            <p class="wow cssanimation fadeInBottom" data-wow-delay="1s">A humildade exprime uma das raras certezas de que estou certo: a de que ninguém é superior a ninguém.</p>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Junte-se a nós</a>
                            <a href="#" class="default-btn wow cssanimation fadeInBottom" data-wow-delay="0.8s">Nossas Turmas</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /#slider-caption-3 -->
    </div>
</section><!-- /#slider-Section -->

<section class="promo-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="row promo-wrap">
                    <div class="col-md-4">
                        <div class="promo-content">
                            <div class="promo-thumb">
                                <img src="img/icon-1.png" alt="icon">
                            </div>
                            <div class="promo-text">
                                <h3>Programa de Bolsas</h3>
                                <p>Faça sua inscrição online</p>
                                <a href="#" class="read-more">Ver mais</a>
                            </div>
                        </div>
                    </div><!-- /#promo-1 -->
                    <div class="col-md-4">
                        <div class="promo-content">
                            <div class="promo-thumb">
                                <img src="img/icon-2.png" alt="icon">
                            </div>
                            <div class="promo-text">
                                <h3>Novos alunos</h3>
                                <p>Faça sua reserva</p>
                                <a href="#" class="read-more">Ver mais</a>
                            </div>
                        </div>
                    </div><!-- /#promo-1 -->
                    <div class="col-md-4">
                        <div class="promo-content">
                            <div class="promo-thumb">
                                <img src="img/icon-3.png" alt="icon">
                            </div>
                            <div class="promo-text">
                                <h3>Certificações</h3>
                                <p>Veja nossas certificações</p>
                                <a href="#" class="read-more">Ver mais</a>
                            </div>
                        </div>
                    </div><!-- /#promo-1 -->
                </div>
            </div>
        </div>
    </div>
</section><!-- /#Promo-Section -->

<section class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 xs-padding">
                <div class="about-content">
                    <h2>Centro Educacional Construir</h2>
                    <p>Texto sobre o colégio</p>
                    <ul class="about-list">
                        <li>Estimulamos a criatividade</li>
                        <li>Sempre provendo as melhores técnicas de ensino</li>
                        <li>Estamos sempre inovando</li>
                    </ul>
                    <a href="#" class="default-btn">Nossas turmas</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about-bg"></div>
            </div>
        </div>
    </div>
</section><!-- /#About Section -->

<section class="course-section bg-grey padding">
    <div class="container">
        <div class="section-heading mb-40 text-center">
            <h2>Nossas turmas</h2>
            <p>A nossa metodologia prevê a valorização das relações com as pessoas envolvidas no universo escolar, sejam elas educandos ou educadores, adotando práticas emancipadoras e transformadoras da realidade em que se encontram.</p>
        </div>
        <div id="course-carousel" class="course-carousel owl-carousel">
            <div class="course-item">
                <div class="course-thumb">
                    <img src="img/course-1.jpg" alt="course">
                    <a class="enroll-btn" href="#">Matrícula <i class="fa fa-plus"></i></a>
                    <div class="thumb-details">
                        <img src="img/team-1.jpg" alt="img">
                        <!--<div class="thumb-content">
                            <h4>João Feitosa</h4>
                        </div>-->
                    </div>
                </div>
                <div class="course-details">
                    <h3><a href="#">Educação Infantil</a></h3>
                    <p>A nossa proposta para Educação Infantil está pautada na priorização dos direitos da criança à infância.</p>
                    <div class="course-footer">
                        <span><i class="fa fa-group"></i>22</span>
                        <span><i class="fa fa-heart"></i>10</span>
                        <span class="price">R$200</span>
                    </div>
                </div>
            </div><!-- /#item-1 -->
            <div class="course-item">
                <div class="course-thumb">
                    <img src="img/course-2.jpg" alt="course">
                    <a class="enroll-btn" href="#">Matrícula <i class="fa fa-plus"></i></a>
                    <div class="thumb-details">
                        <img src="img/team-2.jpg" alt="img">

                    </div>
                </div>
                <div class="course-details">
                    <h3><a href="#">Ensino Fundamental I</a></h3>
                    <p>Os pilares que norteiam nosso trabalho são os elementos essenciais na formação do individuo: Ser, Saber, Conviver e Fazer.</p>
                    <div class="course-footer">
                        <span><i class="fa fa-group"></i>30</span>
                        <span><i class="fa fa-heart"></i>10</span>
                        <span class="price">$250</span>
                    </div>
                </div>
            </div><!-- /#item-2 -->
            <div class="course-item">
                <div class="course-thumb">
                    <img src="img/course-3.jpg" alt="course">
                    <a class="enroll-btn" href="#">Matrícula <i class="fa fa-plus"></i></a>
                    <div class="thumb-details">
                        <img src="img/team-3.jpg" alt="img">
                        <!--<div class="thumb-content">
                            <h4>Joana Neiva</h4>
                        </div>-->
                    </div>
                </div>
                <div class="course-details">
                    <h3><a href="#">Ensino Fundamental II</a></h3>
                    <p>A entrada na adolescência é sempre marcada por diferentes conflitos, o que cria um ritmo de aprendizagem do jovem com o confronto com o mundo, sentimentos, medos e anseios.</p>
                    <div class="course-footer">
                        <span><i class="fa fa-group"></i>15</span>
                        <span><i class="fa fa-heart"></i>30</span>
                        <span class="price">$279</span>
                    </div>
                </div>
            </div><!-- /#item-3 -->
            <div class="course-item">
                <div class="course-thumb">
                    <img src="img/course-4.jpg" alt="course">
                    <a class="enroll-btn" href="#">Matricula <i class="fa fa-plus"></i></a>
                    <div class="thumb-details">
                        <img src="img/team-4.jpg" alt="img">
                    </div>
                </div>
                <div class="course-details">
                    <h3><a href="#">Esportes</a></h3>
                    <p>As atividades esportivas são extremamente importantes para os seres humanos.</p>
                    <div class="course-footer">
                        <span><i class="fa fa-group"></i>40</span>
                        <span><i class="fa fa-heart"></i>2</span>
                        <span class="price">$75</span>
                    </div>
                </div>
            </div><!-- /#item-4 -->
            <div class="course-item">
                <div class="course-thumb">
                    <img src="img/course-5.jpg" alt="course">
                    <a class="enroll-btn" href="#">Book Now <i class="fa fa-plus"></i></a>
                    <div class="thumb-details">
                        <img src="img/team-1.jpg" alt="img">
                        <div class="thumb-content">
                            <h4>Devid Cameroon</h4>
                        </div>
                    </div>
                </div>
                <div class="course-details">
                    <h3><a href="#">Psd to Html Convart</a></h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
                    <div class="course-footer">
                        <span><i class="fa fa-group"></i>763</span>
                        <span><i class="fa fa-heart"></i>205</span>
                        <span class="price">$149</span>
                    </div>
                </div>
            </div><!-- /#item-5 -->
        </div>
    </div>
</section><!-- /#Course-Section -->

<section class="reg-section padding">
    <div class="container">
        <div class="row reg-wrap">
            <div class="col-lg-8 text-center">
                <div class="reg-content">
                    <h3 class="counter">45</h3>
                    <h2>Alunos cadastrados</h2>
                    <h3>Vagas limitadas. Cadastre-se agora<br> e concorra a uma bolsa de estudos</h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="reg-form">
                    <div class="form-heading">
                        <h2>Cadastre-se agora <span>Enviaremos o formulário de inscrição</span></h2>
                    </div>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="phone" placeholder="Telefone">
                        </div>
                        <button type="submit" class="btn btn-primary">Cadastre-se</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- Register Section -->

<section class="event-section bg-grey padding">
    <div class="container">
        <div class="section-heading mb-40 text-center">
            <h2>Eventos</h2>
            <p>Eventos recentes na nossa escola</p>
        </div>
        <div class="event-items">
            <div class="row event-wrap d-flex align-items-center">
                <div class="col-lg-2 text-center">
                    <div class="event-date">
                        <h2>15 de <span>Novembro</span></h2>
                    </div>
                    <div class="ticket-btn">
                        <a href="#" class="default-btn">Venha conferir</a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="event-details">
                        <h2><a href="#">Dia da Proclamação da República</a></h2>
                        <ul class="event-time">
                            <li><i class="fa fa-clock-o"></i>10:00 - 14:00</li>
                            <li><i class="fa fa-map-marker"></i>Auditório principal</li>
                        </ul>
                        <p>O evento aconteceu no Rio de Janeiro, a então capital do país, por um grupo de militares liderado pelo Marechal Deodoro da Fonseca, que deu um golpe de estado no Império.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="event-thumb">
                        <img src="img/course-1.jpg" alt="event">
                    </div>
                </div>
            </div><!-- Event-1 -->
            <div class="row event-wrap d-flex align-items-center">
                <div class="col-lg-2 text-center">
                    <div class="event-date">
                        <h2>20 <span>Novembro</span></h2>
                    </div>
                    <div class="ticket-btn">
                        <a href="#" class="default-btn">Venha conferir</a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="event-details">
                        <h2><a href="#">Dia nacional da Consciência Negra.</a></h2>
                        <ul class="event-time">
                            <li><i class="fa fa-clock-o"></i>08:00 - 12:00</li>
                            <li><i class="fa fa-map-marker"></i>Pátio da escola</li>
                        </ul>
                        <p>
                            O Dia da Consciência Negra é comemorado em 20 de novembro em todo o país. A data homenageia Zumbi, um africano que nasceu livre, mas foi escravizado aos seis anos de idade.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="event-thumb">
                        <img src="img/course-2.jpg" alt="event">
                    </div>
                </div>
            </div><!-- Event-2 -->
        </div>
    </div>
</section><!-- Event Section -->

<section id="counter" class="counter-section">
    <div class="container">
        <ul class="row counters">
            <li class="col-lg-3 col-sm-6 sm-padding">
                <div class="counter-content">
                    <i class="ti-user"></i>
                    <h3><span class="counter">95</span>Instructors</h3>
                </div>
            </li>
            <li class="col-lg-3 col-sm-6 sm-padding">
                <div class="counter-content">
                    <i class="ti-book"></i>
                    <h3><span class="counter">549</span>Online Courses</h3>
                </div>
            </li>
            <li class="col-lg-3 col-sm-6 sm-padding">
                <div class="counter-content">
                    <i class="ti-tag"></i>
                    <h3><span class="counter">205</span>Year of History</h3>
                </div>
            </li>
            <li class="col-lg-3 col-sm-6 sm-padding">
                <div class="counter-content">
                    <i class="ti-crown"></i>
                    <h3><span class="counter">4950</span>Active Students</h3>
                </div>
            </li>
        </ul>
    </div>
</section><!-- Counter Section -->

<section class="testimonial-section padding">
    <div class="container">
        <div class="section-heading mb-40 text-center">
            <h2>O que nossos alunos falam</h2>
            <p>Depoimentos de nossos estudantes</p>
        </div>
        <div id="testimonial-carousel" class="testimonial-carousel owl-carousel">
            <div class="testimonial-item text-center">
                <img src="img/team-1.jpg" alt="profile">
                <h4>João Silva <span>1° ano</span></h4>
                <p>Aprendo coisas novas todos os dias <br> e me sinto muito bem!<br>
                <ul class="rattings">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
            </div><!-- Review-1-->
            <div class="testimonial-item text-center">
                <img src="img/team-2.jpg" alt="profile">
                <h4>Amélia Santos<span>2° ano</span></h4>
                <p>Este ano eu aprendi muito. <br> Gosto de meus colegas e professores<br></p>
                <ul class="rattings">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
            </div><!-- Review-2-->
            <div class="testimonial-item text-center">
                <img src="img/team-3.jpg" alt="profile">
                <h4>Jonathan Smith<span>Wordpress Inc.</span></h4>
                <p>This should be used to tell a story <br> and let your users know about your <br>product or service.</p>
                <ul class="rattings">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
            </div><!-- Review-3-->
            <div class="testimonial-item text-center">
                <img src="img/team-4.jpg" alt="profile">
                <h4>Angelina Rose<span>Envato Inc.</span></h4>
                <p>This should be used to tell a story <br> and let your users know about your <br>product or service.</p>
                <ul class="rattings">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
            </div><!-- Review-4-->
        </div>
    </div>
</section><!-- Testimonial Section -->

<section class="video-cta">
    <div class="video-bg" data-property="{videoURL:'ziysTLL5dig',containment:'self',autoPlay:true, mute:true, startAt:0, opacity:1, showControls:false, ratio:'16/9', quality: 'hd720', showYTLogo: false }"></div>
    <div class="video-content-wrapper">
        <div class="container">
            <div class="video-content text-center">
                <h3>Estímulo a produção de texto</h3>
                <h2>Melhorar na redação?</h2>
                <a href="#" class="default-btn">Veja como</a>
            </div>
        </div>
    </div>
</section><!-- Video Section -->

<section class="blog-section bg-grey padding">
    <div class="container">
        <div class="section-heading mb-40 text-center">
            <h2>Recent Stories</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and industry.</p>
        </div>
        <div class="row">
            <div class="col-lg-12 xs-padding">
                <div class="blog-items grid-list row">
                    <div class="col-md-4 padding-15">
                        <div class="blog-post">
                            <img src="img/post-1.jpg" alt="blog post">
                            <div class="blog-content">
                                <span class="date"><i class="fa fa-clock-o"></i> January 01.2018</span>
                                <h3><a href="#">Standard gallery post</a></h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <a href="#" class="post-meta">Read More</a>
                            </div>
                        </div>
                    </div><!-- Post 1 -->
                    <div class="col-md-4 padding-15">
                        <div class="blog-post">
                            <img src="img/post-2.jpg" alt="blog post">
                            <div class="blog-content">
                                <span class="date"><i class="fa fa-clock-o"></i> January 01.2018</span>
                                <h3><a href="#">Blog post with couple photos</a></h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <a href="#" class="post-meta">Read More</a>
                            </div>
                        </div>
                    </div><!-- Post 2 -->
                    <div class="col-md-4 padding-15">
                        <div class="blog-post">
                            <img src="img/post-3.jpg" alt="blog post">
                            <div class="blog-content">
                                <span class="date"><i class="fa fa-clock-o"></i> January 01.2018</span>
                                <h3><a href="#">Standard gallery post</a></h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <a href="#" class="post-meta">Read More</a>
                            </div>
                        </div>
                    </div><!-- Post 3 -->
                </div>
            </div><!-- Blog Posts -->
        </div>
    </div>
</section><!-- Blog Section -->

<div class="sponsor-section bd-bottom">
    <div class="container">
        <ul id="sponsor-carousel" class="sponsor-items owl-carousel">
            <li class="sponsor-item">
                <img src="img/sponsor-1.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-2.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-3.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-4.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-5.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-6.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-7.png" alt="sponsor-image">
            </li>
            <li class="sponsor-item">
                <img src="img/sponsor-8.png" alt="sponsor-image">
            </li>
        </ul>
    </div>
</div><!-- ./Sponsor Section -->



