<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="<?=Yii::$app->params['description']?>">
    <meta name="author" content="Danilo Santana">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?=Url::base()?>/img/favicon.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="<?=Url::base()?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<?php $this->beginBody() ?>
<!--[if lt IE 8]>
<p class="browserupgrade">Você está usando um browser <strong>antigo</strong>. Por favor <a href="http://browsehappy.com/">atualize seu browser</a> to improve your experience.</p>
<![endif]-->
<div class="site-preloader-wrap">
    <div class="spinner"></div>
</div><!-- Preloader -->

<header id="header" class="header-section">
    <div class="top-header">
        <div class="container">
            <div class="top-content-wrap row">
                <div class="col-md-8">
                    <ul class="left-info">
                        <li><a href="#"><i class="ti-email"></i>ceconstruir@gmail.com</a></li>
                        <li><a href="#"><i class="ti-mobile"></i>(71) 3305-3309</a></li>
                        <li>Contate-nos</li>
                    </ul>
                </div>
                <div class="col-md-4 d-none d-md-block">
                    <ul class="right-info">
                        <li><a href="#">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <div class="container">
            <div class="bottom-content-wrap row">
                <div class="col-md-3">
                    <div class="site-branding">
                        <a href="index.html"><img src="<?=Url::base()?>/img/logo.png" alt="Brand"></a>
                    </div>
                </div>
                <div class="col-md-9 d-none d-md-block text-right">
                    <ul id="mainmenu" class="nav navbar-nav nav-menu">
                        <li class="active"> <a href="<?= Url::home()?>">Home</a></li>
                        <li><a href="<?=Url::to(['site/sobre'])?>">Sobre</a></li>
                        <li><a href="<?=Url::to(['site/cursos'])?>">Cursos</a></li>
                        <li><a href="<?=Url::to(['site/esportes'])?>">Esportes</a></li>

                        <li><a href="<?=Url::to(['site/contato'])?>">Contato</a></li>
                    </ul>
                    <a href="#" class="default-btn">Faça sua Pré matrícula</a>
                </div>
            </div>
        </div>
    </div>
</header><!-- /Header Section -->
<div class="header-height"></div>


        <?= $content?>


<section class="widget-section padding">
    <div class="container">
        <div class="widget-wrap row">
            <div class="col-md-3 xs-padding">
                <div class="widget-content">
                    <img src="img/logo.png" alt="logo">
                    <p>O nome da escola faz alusão a como acontece o processo de ensino-aprendizagem o qual se da através da construção de conhecimentos.
                        A escola surgiu a partir da busca de realização do sonho de vencer barreiras que inviabilizam a construção de uma escola que eduque para o exercício da cidadania e seja instrumento real de transformação social, espaço em que se aprenda a aprender, a conviver e a ser, contrapondo-se ao modelo gerador de desigualdades e exclusão social.</p>
                    <ul class="social-icon">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 xs-padding">
                <div class="widget-content">
                    <h3>Navegar</h3>
                    <ul class="widget-link">
                        <li><a href="#">Sobre nós</a></li>
                        <li><a href="#">Cursos</a></li>
                        <li><a href="#">Esportes</a></li>
                        <li><a href="#">Contate-nos</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 xs-padding">
                <div class="widget-content">
                    <h3>Cursos</h3>
                    <ul class="widget-link">
                        <li><a href="#">Educação Infantil</a></li>
                        <li><a href="#">Ensino Fundamental I</a></li>
                        <li><a href="#">Ensino Fundamental II</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section><!-- ./Widget Section -->
<footer class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 sm-padding">
                <div class="copyright">&copy; <?= date('Y')?> desenvolvido por daniloxc@msn.com</div>
            </div>
            <div class="col-md-6 sm-padding">
                <ul class="footer-social">
                    <li><a href="#">Orçamentos</a></li>
                    <li><a href="#">Informe um bug</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer><!-- /Footer Section -->
<a data-scroll href="#header" id="scroll-to-top"><i class="arrow_up"></i></a>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
