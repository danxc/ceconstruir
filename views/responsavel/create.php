<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Responsavel */

$this->title = 'Create Responsavel';
$this->params['breadcrumbs'][] = ['label' => 'Responsavels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="responsavel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
