<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Responsavel */

$this->title = 'Update Responsavel: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Responsavels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="responsavel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
