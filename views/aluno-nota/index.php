<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AlunoNotaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Aluno Notas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aluno-nota-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Aluno Nota', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'matricula_id',
            'materia_id',
            'nota',
            'unidade',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
