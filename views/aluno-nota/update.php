<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlunoNota */

$this->title = 'Update Aluno Nota: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Aluno Notas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aluno-nota-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
