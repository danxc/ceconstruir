<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190129_204855_create_initial_tables
 */
class m190129_204855_create_initial_tables extends Migration
{



    public function up()
    {
        $this->createTable('aluno', [
            'id' => Schema::TYPE_PK,
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'data_nascimento' => Schema::TYPE_DATE. ' NOT NULL',
            'endereco' => Schema::TYPE_TEXT.' NOT NULL',
            'telefone' => Schema::TYPE_STRING. ' NOT NULL',
            'email' => Schema::TYPE_STRING. ' NOT NULL'
        ]);


        $this->createTable('curso', [
            'id' => Schema::TYPE_PK,
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'requisito' => Schema::TYPE_STRING. ' NOT NULL',
            'descricao' => Schema::TYPE_TEXT.' NOT NULL',
            'telefone' => Schema::TYPE_DOUBLE. ' NOT NULL'
        ]);


        $this->createTable('turma', [
            'id' => Schema::TYPE_PK,
            'data_inicio' => Schema::TYPE_DATE. ' NOT NULL',
            'data_final' => Schema::TYPE_DATE. ' NOT NULL',
            'carga_horaria' => Schema::TYPE_INTEGER. ' NOT NULL',
            'curso_id' => Schema::TYPE_INTEGER. ' NOT NULL',
        ]);

        // creates index for column `curso_id`
        $this->createIndex(
            'idx-turma-curso_id',
            'turma',
            'curso_id'
        );

        // add foreign key for table `curso`
        $this->addForeignKey(
            'fk-turma_curso_id',
            'turma',
            'curso_id',
            'curso',
            'id',
            'CASCADE'
        );


        $this->createTable('matricula', [
            'id' => Schema::TYPE_PK,
            'turma_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'aluno_id' => Schema::TYPE_INTEGER. ' NOT NULL',
            'data' => Schema::TYPE_DATE. ' NOT NULL',
        ]);

        // creates index for column `aluno_id`
        $this->createIndex(
            'idx-matricula-aluno_id',
            'matricula',
            'aluno_id'
        );

        // add foreign key for table `aluno`
        $this->addForeignKey(
            'fk-matricula_aluno_id',
            'matricula',
            'aluno_id',
            'aluno',
            'id',
            'CASCADE'
        );


        // creates index for column `aluno_id`
        $this->createIndex(
            'idx-matricula-turma_id',
            'matricula',
            'turma_id'
        );

        // add foreign key for table `aluno`
        $this->addForeignKey(
            'fk-matricula_turma_id',
            'matricula',
            'turma_id',
            'turma',
            'id',
            'CASCADE'
        );

        $this->createTable('materia', [
            'id' => Schema::TYPE_PK,
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'descricao' => Schema::TYPE_TEXT.' NOT NULL',
        ]);


        $this->createTable('aluno_nota', [
            'id' => Schema::TYPE_PK,
            'matricula_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'materia_id' => Schema::TYPE_INTEGER. ' NOT NULL',
            'nota' => Schema::TYPE_DOUBLE.' NOT NULL',
            'unidade' => Schema::TYPE_INTEGER. ' NOT NULL'
        ]);


        // creates index for column `materia_id`
        $this->createIndex(
            'idx-aluno_nota-materia_id',
            'aluno_nota',
            'materia_id'
        );

        // add foreign key for table `materia`
        $this->addForeignKey(
            'fk-aluno_nota_materia_id',
            'aluno_nota',
            'materia_id',
            'materia',
            'id',
            'CASCADE'
        );

        // creates index for column `matricula_id`
        $this->createIndex(
            'idx-aluno_nota-matricula_id',
            'aluno_nota',
            'matricula_id'
        );

        // add foreign key for table `matricula`
        $this->addForeignKey(
            'fk-aluno_nota_matricula_id',
            'aluno_nota',
            'matricula_id',
            'matricula',
            'id',
            'CASCADE'
        );



        $this->createTable('responsavel', [
            'id' => Schema::TYPE_PK,
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'empresa' => Schema::TYPE_STRING . ' NOT NULL',
            'aluno' => Schema::TYPE_INTEGER. ' NOT NULL',
            'endereco' => Schema::TYPE_TEXT.' NOT NULL',
            'telefone' => Schema::TYPE_STRING. ' NOT NULL',
            'rg' => Schema::TYPE_STRING . ' NOT NULL',
            'cpf' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING. ' NOT NULL'
        ]);

        // creates index for column `aluno_id`
        $this->createIndex(
            'idx-responsavel-aluno_id',
            'responsavel',
            'aluno'
        );

        // add foreign key for table `aluno`
        $this->addForeignKey(
            'fk-responsavel_aluno_id',
            'responsavel',
            'aluno',
            'aluno',
            'id',
            'CASCADE'
        );



    }

    public function down()
    {
        // responsavel,aluno_nota,materiamatricula, turma, curso aluno
        try {
            $this->dropTable('responsavel');
            $this->dropTable('aluno_nota');
            $this->dropTable('materia');
            $this->dropTable('matricula');
            $this->dropTable('turma');
            $this->dropTable('curso');
            $this->dropTable('aluno');
            echo "m190129_204855_create_initial_tables reverted.\n";
        }
        catch(Exception $e){
            echo "m190129_204855_create_initial_tables cannot be reverted.\n";
            return false;
        }




    }

}
