<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/font-awesome.min.css',
        'css/themify-icons.css',
        'css/elegant-font-icons.css',
        'css/elegant-line-icons.css',
        'css/bootstrap.min.css',
        'css/venobox/venobox.css',
        'css/owl.carousel.css',
        'css/slicknav.min.css',
        'css/css-animation.min.css',
        'css/nivo-slider.css',
        'css/main.css',
        'css/responsive.css',
    ];

    public $js = [
        //'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
        'js/vendor/jquery-1.12.4.min.js',
        //'js/vendor/bootstrap.min.js',
        'js/vendor/tether.min.js',
        'js/vendor/imagesloaded.pkgd.min.js',
        'js/vendor/owl.carousel.min.js',
        'js/vendor/jquery.isotope.v3.0.2.js',
        'js/vendor/smooth-scroll.min.js',
        'js/vendor/venobox.min.js',
        'js/vendor/jquery.ajaxchimp.min.js',
        'js/vendor/jquery.counterup.min.js',
        'js/vendor/jquery.waypoints.v2.0.3.min.js',
        'js/vendor/jquery.slicknav.min.js',
        'js/vendor/jquery.nivo.slider.pack.js',
        'js/vendor/jquery.mb.YTPlayer.min.js',
        'js/vendor/wow.min.js',
        'js/contact.js',
        'js/main.js'
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
